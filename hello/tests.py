# django
from django.test import TestCase, Client
import time

# selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class StatusTest(TestCase):

    # Apakah localhost:8000 bisa diakses
    def test_url_apakah_bisa_dibuka(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

class FunctionalTest(TestCase):
    # Aktivasi browser
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        # self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    # Tutup browser, hapus data percobaan dari models
    # lalu tunggu, baru tutup
    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_functional_bisa_ganti_tema_dan_pake_accordion(self):
        # Willy buka websitenya di Chrome
        self.browser.get('http://localhost:8000')

        # Title yang Willy lihat adalah (title here)
        self.assertIn('title here', self.browser.title)
        time.sleep(1)

        # Willy memencet tombol ganti tema
        # dan website jadi dark mode

        # Willy klik lagi dan kembali seperti semula

        # Willy mengeklik accordion deskripsi diri
        # dan isi deskripsinya muncul kebawah

        # Oke websitenya bekerja dengan baik.
    