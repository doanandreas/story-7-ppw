$(document).ready(()=>{
    let dark_toggle = false;

    console.log("masuk")
    var checkbox = document.querySelector('input[type=checkbox]');

    $(checkbox).prop("checked", false);

    checkbox.addEventListener('click', function(){
        if(!dark_toggle){
            $('body').removeClass("bg-light").addClass("bg-dark");
            dark_toggle = !dark_toggle;
        }else{
            $('body').removeClass("bg-dark").addClass("bg-light");
            dark_toggle = !dark_toggle;
        }
    })

    $(function(){
        $("#accordion").accordion({
            collapsible: true
        });
    });
})